import urllib

class Parser: 
    NotFound = "NotFound"
    Turkcealtyazi = "Turkcealtyazi"
    Gsmarena = "Gsmarena"
    Gezginler = "Gezginler"
    Alternatifim = "Alternatifim"
    Genius = "Genius"
    EpeyUrun = "EpeyUrun"
    AkakceUrun = "AkakceUrun"
    Eksi = "Eksi"

def checkParser(res):
    print 'len(res["result"])', len(res["result"])
    for i in res["result"]:
        url = i["url"]
        print "++", url
        if url.startswith("https://www.turkcealtyazi.org/sub/"):
            parserurl = "/turkcealtyazi/" + urllib.quote(url, safe="")
            return Parser.Turkcealtyazi, url, parserurl
        if url.startswith("https://www.gezginler.net/indir") and url.endswith(".html"):
            parserurl = "/gezginler/" + urllib.quote(url, safe="")
            return Parser.Gezginler, url, parserurl
        if url.startswith("https://sarki.alternatifim.com/sarkici") or url.startswith("https://ceviri.alternatifim.com/sarkici"):
            parserurl = "/alternatifim/" + urllib.quote(url, safe="")
            return Parser.Alternatifim, url, parserurl
        if url.startswith("https://genius.com/") and url.endswith("lyrics"):
            parserurl = "/genius/" + urllib.quote(url, safe="")
            return Parser.Genius, url, parserurl
        if url.startswith("https://www.epey.com/") and url.endswith("html"):
            parserurl = "/epeyurun/" + urllib.quote(url, safe="")
            return Parser.EpeyUrun, url, parserurl
        if url.startswith("https://www.akakce.com/") and url.endswith("html") and len(url.split("/")) >= 5:
            parserurl = "/akakceurun/" + urllib.quote(url, safe="")
            return Parser.AkakceUrun, url, parserurl
        if url.startswith("https://eksisozluk.com/"):
            parserurl = "/eksi/" + urllib.quote(url, safe="")
            return Parser.Eksi, url, parserurl
    return Parser.NotFound, "", ""

def checkYoutube(res):
    for i in res["result"]:
        url = i["url"]
        if url.startswith("https://www.youtube.com/watch") and "v=" in url:
            vid = url.split("v=")[1]
            return url, vid
        if url.startswith("https://www.youtube.com/watch/"):
            vid = url.replace("https://www.youtube.com/watch/", "")
            return url, vid
    return False, False
    