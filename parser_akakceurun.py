import requests
import urllib, urlparse
import re
try: 
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup

def parse(url):
    response = requests.get(url, headers=headers)
    html = response.content #.decode("ISO-8859-9")
    soup = BeautifulSoup(html, 'html.parser')
    
    title = soup.select_one('title').contents[0]
    el_block = soup.select_one('ul#PL')

    urlbase = "https://www.akakce.com"
    for link in el_block.find_all('a'):
        href = link["href"].split("?")[1]
        href = "/c/?" + href

        link["href"] = urlparse.urljoin(urlbase, href)
        link["target"] = "_blank"

    block = unicode(el_block)
    h1 = soup.h1.text
    # adverts = lyrics(['script', 'style', 'div'])
    # for tag in adverts: tag.decompose()

    return {
        "url": url,
        "title": title,
        "block": block,
        "h1": h1
    }

headers = {
    'Connection': 'keep-alive',
    'Cache-Control': 'max-age=0',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36 OPR/58.0.3135.132',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'Referer': 'http://127.0.0.1:5000/ara/en+ucuz+iphone+xr/',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'en-US,en;q=0.9,tr;q=0.8',
}

# response = requests.get('https://www.akakce.com/cep-telefonu/en-ucuz-iphone-xr-64gb-fiyati,330748104.html', headers=headers)


if __name__ == "__main__":
    print "start"
    parse("https://www.akakce.com/cep-telefonu/en-ucuz-iphone-xr-64gb-fiyati,330748104.html")
    print "end"
