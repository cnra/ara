import requests
import urllib
import re
try: 
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup

def parserGoruntule(urlquoted):
    r = parse(urlquoted)
    fullname, icon, downloadUrl = r["fullname"], r["icon"], r["downloadUrl"]
    
    return """
    <h3>{fullname}</h3>
    <img src="{icon}" height="128" width="128" />
    <a href="{downloadUrl}"  target="_blank"><img src="/static/indir.png"></a><br>
    <a href="{downloadUrl}" target="_blank">{downloadUrl}</a>
    
    """.format(**locals())
 
def parse(url):
    # url = 'https://www.gezginler.net/indir/gom-player.html'
    response = requests.get(url, headers=headers)
    # print response.content
    # response.raw.decode_content = True
    html = response.content.decode("ISO-8859-9")
    soup = BeautifulSoup(html, 'html.parser')
    
    title = soup.select_one('title').contents[0]
    icon = soup.select_one('div#baslik img')
    icon = icon["src"] if icon else None

    tarih = re.findall("tarih = '(.*)'", html)[0]
    print 'tarih:', tarih
    linkbase = re.findall('url = "(.*?)"', html)[0]
    print 'linkbase:', linkbase
    link = linkbase + tarih + "/"
    link = link.replace("http://", "https://")
    desc = soup.find(itemprop="description").text if soup.find(itemprop="description") else None
    print 'link:', link
    size = ""
    try:
        # soup.findAll("div", class_ = "dt4")
        size = soup.select(".dt4")[0].contents[1].replace(": ", "")
    except:
        print "size bulunamadi"

    names = soup.findAll(itemprop="name")
    publisher = names[1].text if names else None
    fullname = soup.h1.text if soup.h1 else None
        
    # linki takip edip harici indirme linkini bulalim
    res = requests.get(link, headers=headers, allow_redirects=False)
    downloadUrl = res.headers["Location"]


    print "downloadUrl: ", downloadUrl
    return {
        "icon": icon,
        "title": title,
        "fullname": fullname,
        "publisher": publisher,
        "desc": desc,
        "url": url,
        "size": size,
        "url_redirect": link,
        "downloadUrl": downloadUrl,
        "status": "ok",
    }

    return '<a href="{}" target="_blank">{}</a>'.format(downloadUrl,downloadUrl)
    return html


headers = {
    'authority': 'www.gezginler.net',
    'cache-control': 'no-cache',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36 OPR/58.0.3135.132',
    'accept': 'image/webp,image/apng,image/*,*/*;q=0.8',
 #   'referer': 'https://www.gezginler.net/indir/vlc-player.html',
    # 'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'en-US,en;q=0.9,tr;q=0.8',
    'cookie': '__cfduid=d508a53900b655becc327334021a2a10b1544534763; _ym_uid=1557475400590599141; _ym_d=1557475400; __utmc=182196785; _ym_isad=2; __utma=182196785.1335753638.1557475400.1557475400.1557484364.2; __utmz=182196785.1557484364.2.2.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); __utmt=1; _ym_visorc_13121794=w; __utmb=182196785.4.9.1557484773924',
 #   'Referer': 'https://www.gezginler.net/indir/vlc-player.html',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36 OPR/58.0.3135.132',
    'if-none-match': '"ef16515653cd75f3154672b86ac100b8"',
    # 'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'en-US,en;q=0.9,tr;q=0.8',
    'Accept': '*/*',
    'If-None-Match': '"5cc6c534-9b15"',
    'Connection': 'keep-alive',
    'If-Modified-Since': 'Mon, 29 Apr 2019 09:34:44 GMT',
    'Origin': 'https://www.gezginler.net',
    'Content-Type': 'application/x-www-form-urlencoded',
    'Content-Length': '0',
    'pragma': 'no-cache',
}

if __name__ == "__main__":
    print "bla"
    parse("https://www.gezginler.net/indir/adobe-acrobat-reader-dc.html")
    print "bla2"
    print "bla3"