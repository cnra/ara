import requests
import urllib
import re
try: 
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup

def parse(url):
    response = requests.get(url, headers=headers)
    html = response.content #.decode("ISO-8859-9")
    soup = BeautifulSoup(html, 'html.parser')
    
    title = soup.select_one('title').contents[0]
    lyrics = soup.select_one('.lyrics')
    h1 = soup.h1.contents[0]
    # adverts = lyrics(['script', 'style', 'div'])
    # for tag in adverts: tag.decompose()
    
    lyric = lyrics.p.text.replace("\n", "<br/>")
    return {
        "url": url,
        "title": title,
        "lyric": lyric,
        "h1": h1
    }


headers = {
    'authority': 'genius.com',
    'cache-control': 'max-age=0',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36 OPR/58.0.3135.132',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'referer': 'https://genius.com/',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'en-US,en;q=0.9,tr;q=0.8',
    'cookie': '__cfduid=dd7cb86e72f055ec3a974eecd3d433d5e1557644618; _ab_tests_identifier=07f155c3-f910-4b53-b33d-6e537daeb09e; AMP_TOKEN=%24NOT_FOUND; _ga=GA1.2.1875793852.1557644621; _gid=GA1.2.1734920727.1557644621; __qca=P0-126013778-1557644620548; mp_mixpanel__c=0; _cb_ls=1; _cb=BfAfvOoqFU9BrEwnC; _chartbeat2=.1557644623189.1557644623189.1.CRshL6BVG90IDNcp-8d73zpFJ3kW.1; _cb_svref=https%3A%2F%2Fwww.google.com.tr%2F; mp_77967c52dc38186cc1aadebdd19e2a82_mixpanel=%7B%22distinct_id%22%3A%20%221875793852.1557644621%22%2C%22%24device_id%22%3A%20%2216aaadab38a5d-00ef788c9c3d83-4d321a53-384000-16aaadab38b1d1%22%2C%22Logged%20In%22%3A%20false%2C%22Is%20Editor%22%3A%20null%2C%22Is%20Moderator%22%3A%20null%2C%22Mobile%20Site%22%3A%20false%2C%22AMP%22%3A%20false%2C%22Tag%22%3A%20%22pop%22%2C%22genius_platform%22%3A%20%22web%22%2C%22%24search_engine%22%3A%20%22google%22%2C%22%24initial_referrer%22%3A%20%22https%3A%2F%2Fwww.google.com.tr%2F%22%2C%22%24initial_referring_domain%22%3A%20%22www.google.com.tr%22%2C%22%24user_id%22%3A%20%221875793852.1557644621%22%7D; flash=%7B%7D; _csrf_token=q%2FKT99%2BopX4WEHGiHeLsX8S8volPTjABu5AAz%2Fc6Ra0%3D; _rapgenius_session=BAh7BzoPc2Vzc2lvbl9pZEkiJWY1M2MxZDI0MGUzYWM2OWQyOTc1NDg4NmRjZmUyNjYzBjoGRUY6EF9jc3JmX3Rva2VuSSIxcS9LVDk5K29wWDRXRUhHaUhlTHNYOFM4dm9sUFRqQUJ1NUFBei9jNlJhMD0GOwZG--b4d0677a67d4865c6f956a9167e64b1050a333d6',
}

# params = (
#     ('referent_id', '10512786'),
# )

# response = requests.get('https://genius.com/Grimes-laughing-and-not-being-normal-lyrics', headers=headers, params=params)


if __name__ == "__main__":
    print "start"
    parse("https://genius.com/Grimes-laughing-and-not-being-normal-lyrics")
    print "end"
