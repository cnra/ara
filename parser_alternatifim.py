import requests
import urllib
import re
try: 
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup

def parse(url):
    response = requests.get(url, headers=headers)
    html = response.content #.decode("ISO-8859-9")
    soup = BeautifulSoup(html, 'html.parser')
    
    title = soup.select_one('title').contents[0]
    lyrics = soup.select_one('.sarkisozu')
    h1 = soup.h1.contents[0]
    adverts = lyrics(['script', 'style', 'div'])
    for tag in adverts: tag.decompose()
    
    lyric = lyrics.decode_contents()
    return {
        "url": url,
        "title": title,
        "lyric": lyric,
        "h1": h1
    }


headers = {
    'authority': 'sarki.alternatifim.com',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36 OPR/58.0.3135.132',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'en-US,en;q=0.9,tr;q=0.8',
    'cookie': 'ezux_et_71024=7; ezovuuidtime_71024=1557641687; ezux_lpl_71024=1557641689509|8c2bdaea-7151-40c6-77bd-76e2570a32ea; ezux_tos_71024=68; ezoadgid_71024=-1; ezoref_71024=; ezoab_71024=mod21; ezepvv=55; lp_71024=https://sarki.alternatifim.com/sarkici/sebnem-ferah/sigara; ezovid_71024=1114761984; ezovuuid_71024=9915a8a7-3ed3-4e6e-62d8-d0e87d795602; ezCMPCCS=true; ezouspvv=0; ezouspva=0; _ga=GA1.2.2130830746.1557641616; _gid=GA1.2.485178340.1557641616; __utma=67787201.2130830746.1557641616.1557641616.1557641616.1; __utmc=67787201; __utmz=67787201.1557641616.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utmt_e=1; __utmt_f=1; uid-s=$832f175-66cd-4ff9-af12-94e176a12bd5; vsid=e4fa58bb-ae28-48c9-8701-0b1b21d9cdc9; ezds=ffid%3D1%2Cw%3D2560%2Ch%3D1440; active_template::71024=pub_site.1557641686; ezopvc_71024=2; __utmb=67787201.10.8.1557641676286; _gat_gtag_UA_17590819_1=1; ezohw=w%3D922%2Ch%3D1287',
}

if __name__ == "__main__":
    print "start"
    parse("https://sarki.alternatifim.com/sarkici/sebnem-ferah/hoscakal")
    print "end"
