import requests
import urllib, urlparse
import re
try: 
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup

def parse(url):
    print "eksiurl: ", url
    response = requests.get(url, headers=headers)
    html = response.content #.decode("ISO-8859-9")
    soup = BeautifulSoup(html, 'html.parser')
    
    title = soup.select_one('title').contents[0]
    el_block = soup.select('div.content')
    h1 = soup.h1.text

    maxel = None
    maxlen = 0
    for el in el_block:
        el = fixlinks(el, "https://eksisozluk.com")
        if len(el.text) > maxlen:
            maxlen = len(el.text)
            maxel = el

    block = maxel.decode_contents()

    return {
        "url": url,
        "title": title,
        "block": block,
        "h1": h1
    }


def fixlinks(el, urlbase):
    for link in el.find_all('a'):
        link["href"] = urlparse.urljoin(urlbase, link["href"])
        link["target"] = "_blank"
    return el
    

headers = {
    'authority': 'eksisozluk.com',
    'cache-control': 'max-age=0',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36 OPR/58.0.3135.132',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'en-US,en;q=0.9,tr;q=0.8',
    'cookie': 'iq=a871d7862225408bbce1c9d27ca458b5; ASP.NET_SessionId=lqex0jonhoctjuqg0kzkz2np; _ga=GA1.2.1410390149.1548928912; __gads=ID=6c1de5a56d04dfe5:T=1548928910:S=ALNI_Mb4dUKrRTVYtXfFj2DjwmwUo_yhLw; __gfp_64b=M0R7wEu5xRIZ62VQL_2nB0kShZvJxEhGArCZl40B_nD.87; __auc=c9f12789168b8b343c233d2de98; _gid=GA1.2.1766767149.1549447485; notheme=1; __RequestVerificationToken=F44IvDjSOmt5OfBlyTfkzr6r0r7NpvoRnnWvfWawP5O_wo4FT6cNylt_XEXC8tqHbK7iFeq3VISj_d3QsQRu-yD9ybKkqq1ExkLymWiXRJo1; alertsnap=636863583944921500; bm_monthly_unique=true; a=w3J1iT2oOrHEzLi+1KR4aPZIbuQaR2Tpp2RNiec18tJlTeWQPg/PM9eKsDrNTC6cc+3mAFadnTNnFRC0+6A6H/Pxo6f1Than+aoTqNCjEXE45XNuAH3+eN7KM0wVc4+DNWA+R6yY9hGhr5wgTFuIA3OXPlqrEnqk/FqG8CZnvyajxMsFNMyhwX84vidLgnN0TO4LlAifq+EHhr7kG5H/7A==; bm_daily_unique=true; bm_sample_frequency=100; sticky_id=8109e130746131083e098062b3f4c064; _gat=1; bm_last_load_status=BLOCKING',
}


if __name__ == "__main__":
    print "start"
    parse("https://eksisozluk.com/trakya%20%C3%BCniversitesi")
    #parse("https://eksisozluk.com/trakya-universitesi--192258")
    #parse("https://eksisozluk.com/redis--2163265")
    print "end"
