import requests
import urllib
import re
try: 
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup

def parse(url):
    response = requests.get(url, headers=headers)
    html = response.content #.decode("ISO-8859-9")
    soup = BeautifulSoup(html, 'html.parser')
    
    title = soup.select_one('title').contents[0]
    el_block = soup.select('.urun > div')[1]
    block = unicode(el_block)
    h1 = soup.h1.text
    # adverts = lyrics(['script', 'style', 'div'])
    # for tag in adverts: tag.decompose()
    
    return {
        "url": url,
        "title": title,
        "block": block,
        "h1": h1
    }


headers = {
    'authority': 'www.epey.com',
    'cache-control': 'max-age=0',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36 OPR/58.0.3135.132',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'referer': 'http://127.0.0.1:5000/ara/s10+%C3%B6zellikler/',
    'accept-language': 'en-US,en;q=0.9,tr;q=0.8',
    'cookie': 'PHPSESSID=17gh89ci1vaqhjg723t8dg8vc1; _ga=GA1.2.979843549.1550429764; _gcl_au=1.1.1669529056.1550429764; fs=source%3Ddirect%26medium%3Dnone%26campaign%3Ddirect%26term%3D%26content%3D%26date%3D20190217; _gid=GA1.2.1450579823.1557650048; rs=source%3D127.0.0.1%3A5000%26medium%3Dreferral%26campaign%3D%26term%3D%26content%3D%26date%3D20190512; __cfduid=d5e3951780ac63c02cb55ab5768cb7f791557436252',
}

if __name__ == "__main__":
    print "start"
    parse("https://www.epey.com/akilli-telefonlar/samsung-galaxy-s10.html")
    print "end"
