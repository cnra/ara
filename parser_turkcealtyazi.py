import requests
import urllib, urlparse
try: 
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup

headers = {
    'authority': 'www.turkcealtyazi.org',
    'cache-control': 'max-age=0',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36 OPR/58.0.3135.132',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'referer': 'https://www.google.com.tr/',
    #'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'en-US,en;q=0.9,tr;q=0.8',
    'cookie': '__cfduid=d9d5d32523793d966d8a4fb09145d73741548944433; phpbb2mysql_data=a%3A2%3A%7Bs%3A11%3A%22autologinid%22%3Bs%3A0%3A%22%22%3Bs%3A6%3A%22userid%22%3Bi%3A-1%3B%7D; _ga=GA1.2.2081579499.1548944436; _gid=GA1.2.1809311472.1557455795; phpbb2mysql_sid=a4b55a560389263b58ec73537f0b955a; _gat=1',
}

headers_dl = {
    'authority': 'www.turkcealtyazi.org',
    'cache-control': 'max-age=0',
    'origin': 'https://www.turkcealtyazi.org',
    'upgrade-insecure-requests': '1',
    'content-type': 'application/x-www-form-urlencoded',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36 OPR/58.0.3135.132',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'referer': 'https://www.turkcealtyazi.org/sub/666697/westworld.html',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'en-US,en;q=0.9,tr;q=0.8',
    'cookie': '__cfduid=d9d5d32523793d966d8a4fb09145d73741548944433; phpbb2mysql_data=a%3A2%3A%7Bs%3A11%3A%22autologinid%22%3Bs%3A0%3A%22%22%3Bs%3A6%3A%22userid%22%3Bi%3A-1%3B%7D; _ga=GA1.2.2081579499.1548944436; phpbb2mysql_sid=a9856f7b1589e32ede4dfb6b180b1501; _gid=GA1.2.1809311472.1557455795',
}

def download_subtitle(url):
    r = parse(url)
    idid, altid, sidid = r["idid"],r["altid"],r["sidid"]

    data = {
        'idid': idid,
        'altid': altid,
        'sidid': sidid
    }

    print data
    print "+ dl start"
    r = requests.post('https://www.turkcealtyazi.org/ind', headers=headers_dl, data=data)
    fileheader = r.headers["Content-disposition"]
    print "- dl end"
    print 'size: ', len(r.content)
    print 'head: ',  fileheader
    
    idx = fileheader.index("filename=") + len("filename=")
    filename = fileheader[idx:]

    return {
        "content": r.content, 
        "filename": filename
    }

def parse(url, baseurl=""):
    response = requests.get(url, headers=headers)
    # response.raw.decode_content = True
    soup = BeautifulSoup(response.content, 'html.parser')

    idid = soup.select_one("input[name='idid']")["value"] if soup.select_one("input[name='idid']") else None
    sidid = soup.select_one("input[name='sidid']")["value"] if soup.select_one("input[name='sidid']") else None
    altid = soup.select_one("input[name='altid']")["value"] if soup.select_one("input[name='sidid']") else None
    print "idid:", idid
    print "sidid:", sidid
    print "altid:", altid

    block = soup.select('div#altyazilar')[0]
    if len(block.find_all('a')) < 1:
        block = block.parent

    urlbase = "https://www.turkcealtyazi.org"
    for link in block.find_all('a'):
        href = link["href"]
        if href and href.startswith("/sub"):
            link["href"] = baseurl + '/turkcealtyazi-indir/' + urlparse.urljoin(urlbase, href)
            print(link["href"])
        else:
            link["href"] = urlparse.urljoin(urlbase, href)
            link["target"] = "_blank"
            
    print "-----"

    downloadUrl = None
    if "/sub/" in url:
        downloadUrl = baseurl + "turkcealtyazi-indir/" + url

    return {
        "block": unicode(block),
        "idid": idid,
        "sidid": sidid,
        "altid": altid,
        "downloadUrl": downloadUrl,
        "baseurl": baseurl
    }

if __name__ == "__main__":
    download_subtitle("https://www.turkcealtyazi.org/sub/368595/another-earth.html")
    print "ok"


