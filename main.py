from flask import Flask, render_template, request, redirect, jsonify, Response, send_file, g
import api_yandex, cache
import parsers, parser_turkcealtyazi, parser_gezginler, parser_alternatifim, parser_genius, parser_epeyurun, parser_akakceurun, parser_eksi
import io, urllib, time
from parsers import Parser
from raven.contrib.flask import Sentry
app = Flask(__name__)
sentry = Sentry(app, dsn='http://81c9fbe93155407f87618fd5f148f839:891d9974918e43fbbad785f75ea34996@sentry.fikir.pw/6')

@app.route("/")
def index():
    query = request.args.get('q')
    if query: return redirect("/ara/"+query.replace(" ", "+"), code=302)
    else: return render_template("home.html")

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/ara/<string:query>/', defaults={'page': 0})
@app.route('/ara/<string:query>/<int:page>')
def ara(query, page):
    ## ++ cache control
    cachekey = u"{}:{}:{}".format(unicode(query),query,page)
    c = cache.get_cache(cachekey)
    res = api_yandex.search(query, page) if not c else c
    if not c: cache.set_cache(cachekey, res)
    ## -- cache control

    ## ++ parsers
    parser, url, parserurl = parsers.checkParser(res)
    urlyt, vid = parsers.checkYoutube(res)
    inputtext = query.replace("+", " ")
    print "ppp:", parser, parserurl,
    print "vid:", vid
    ## -- parsers
    
    pages = range(max(0, page-4), min(page+4, 99)) 
    return render_template('serp.html', 
        result=res["result"], res=res, query=query, 
        parser=parser, parserurl=parserurl,
        page=page, pages=pages, yt_vid=vid,cache=c,
        inputtext=inputtext)

@app.route('/turkcealtyazi/<path:urlquoted>')
def turkcealtyazi_embed(urlquoted):
    print "+++ turkcealtyazi +++"
    url = urllib.unquote(urlquoted)
    ## ++ cache control
    cachekey = u"{}:{}".format("turkcealtyazi",url.replace(":", ""))
    c = cache.get_cache(cachekey)
    res = parser_turkcealtyazi.parse(url) if not c else c
    if not c: cache.set_cache(cachekey, res)
    ## -- cache control

    block = res["block"]
    return render_template("embed_turkcealtyazi.html", block=block, cache=c)

@app.route('/turkcealtyazijson/<path:urlquoted>')
def turkcealtyazi_json(urlquoted):
    print "+++ turkcealtyazi +++"
    url = urllib.unquote(urlquoted)
    r = parser_turkcealtyazi.parse(url, request.host_url)

    return jsonify(r)

@app.route('/turkcealtyazi-indir/<path:urlquoted>')
def turkcealtyaziindir(urlquoted):
    url = urllib.unquote(urlquoted)
    ## ++ cache control
    cachekey = u"{}:{}".format("turkcealtyazi-indir",url.replace(":", ""))
    c = cache.get_cache(cachekey)
    res = parser_turkcealtyazi.download_subtitle(url) if not c else c
    if not c: cache.set_cache(cachekey, res)
    ## -- cache control
    
    content, filename = res["content"], res["filename"]
    return send_file(
        io.BytesIO(content),
        as_attachment=True,
        attachment_filename=filename,
        mimetype='application/octet-stream'
    )
    
@app.route('/gezginler/<path:urlquoted>')
def gezginler_embed(urlquoted):
    print 'gezginler_route'
    url = urllib.unquote(urlquoted)
    ## ++ cache control
    cachekey = u"{}:{}".format("gezginler",url.replace(":", ""))
    c = cache.get_cache(cachekey)
    r = parser_gezginler.parse(url) if not c else c
    if not c: cache.set_cache(cachekey, r)
    ## -- cache control
    
    # fullname, icon, downloadUrl = r["fullname"], r["icon"], r["downloadUrl"], 
    locals().update(r) # r dictionary keylerini local degisken yapiyor
    return render_template('embed_gezginler.html', 
        cache=c, **locals()
    )
    
    # return gezginler.parserGoruntule(url)

@app.route('/gezginlerjson/<path:urlquoted>')
def gezginler_json(urlquoted):
    url = urllib.unquote(urlquoted)
    res = parser_gezginler.parse(url)
    return jsonify(res)

@app.route('/alternatifim/<path:urlquoted>')
def alternatifim_embed(urlquoted):
    print 'alternatifim_embed'
    url = urllib.unquote(urlquoted)
    ## ++ cache control
    cachekey = u"{}:{}".format("alternatifim",url.replace(":", ""))
    c = cache.get_cache(cachekey)
    r = parser_alternatifim.parse(url) if not c else c
    if not c: cache.set_cache(cachekey, r)
    ## -- cache control
    
    lyric, title,h1 = r["lyric"], r["title"], r["h1"]
    locals().update(r) # r dictionary keylerini local degisken yapiyor
    return render_template('embed_alternatifim.html', 
        lyric = lyric, title="title", h1=h1, url=url, cache=c
    )

@app.route('/alternatifim_json/<path:urlquoted>')
def alternatifim_json(urlquoted):
    print "+++ alternatifim_json +++"
    url = urllib.unquote(urlquoted)
    r = parser_alternatifim.parse(url)
    return jsonify(r)

@app.route('/genius/<path:urlquoted>')
def genius_embed(urlquoted):
    print 'alternatifim_embed'
    url = urllib.unquote(urlquoted)
    ## ++ cache control
    cachekey = u"{}:{}".format("genius",url.replace(":", ""))
    c = cache.get_cache(cachekey)
    r = parser_genius.parse(url) if not c else c
    # if not c: cache.set_cache(cachekey, r)
    ## -- cache control
    
    lyric, title,h1 = r["lyric"], r["title"], r["h1"]
    locals().update(r) # r dictionary keylerini local degisken yapiyor
    return render_template('embed_genius.html', 
        lyric = lyric, title=title, h1=h1, url=url, cache=c
    )

@app.route('/genius_json/<path:urlquoted>')
def genius_json(urlquoted):
    print "+++ genius_json +++"
    url = urllib.unquote(urlquoted)
    r = parser_genius.parse(url)
    return jsonify(r)


@app.route('/epeyurun/<path:urlquoted>')
def epeyurun_embed(urlquoted):
    print "+++ epeyurun_embed +++"
    url = urllib.unquote(urlquoted)
    ## ++ cache control
    cachekey = u"{}:{}".format("epeyurun",url.replace(":", ""))
    c = cache.get_cache(cachekey)
    r = parser_epeyurun.parse(url) if not c else c
    if not c: cache.set_cache(cachekey, r)
    ## -- cache control
   
    block, title, h1 = r["block"], r["title"], r["h1"]

    return render_template('embed_epeyurun.html', 
        block = block, title=title, h1=h1, cache=c
    )


@app.route('/epeyurun_json/<path:urlquoted>')
def epeyurun_json(urlquoted):
    print "+++ epeyurun_json +++"
    url = urllib.unquote(urlquoted)
    r = parser_epeyurun.parse(url)
    return jsonify(r)

@app.route('/akakceurun/<path:urlquoted>')
def akakceurun_embed(urlquoted):
    print "+++ akakceurun_embed +++"
    url = urllib.unquote(urlquoted)
    ## ++ cache control
    cachekey = u"{}:{}".format("akakceurun",url.replace(":", ""))
    c = cache.get_cache(cachekey)
    r = parser_akakceurun.parse(url) if not c else c
    if not c: cache.set_cache(cachekey, r)
    ## -- cache control

    block, title, h1 = r["block"], r["title"], r["h1"]

    return render_template('embed_akakceurun.html', 
        block = block, title=title, h1=h1, cache=c
    )


@app.route('/eksi/<path:urlquoted>')
def eksi_embed(urlquoted):
    url = urllib.unquote(urlquoted)
    ## ++ cache control
    cachekey = u"{}:{}".format("eksi",url.replace(":", ""))
    c = cache.get_cache(cachekey)
    r = parser_eksi.parse(urlquoted) if not c else c
    # if not c: cache.set_cache(cachekey, r)
    ## -- cache control
    
    block, title, h1 = r["block"], r["title"], r["h1"]

    return render_template('embed_eksisozluk.html', 
        block = block, title=title, h1=h1,url =urlquoted
    )

@app.route('/eksi_json/<path:urlquoted>')
def eksi_json(urlquoted):
    url = urllib.unquote(urlquoted)
    r = parser_eksi.parse(urlquoted)
    return jsonify(r)


@app.route('/hata')
def hata():
    raise Exception("hata olustu!")


@app.route('/togglecache')
def togglecache():
    cache.toggle_cache()
    c = cache.cache_status()
    ref = request.headers.get("Referer")
    return redirect(ref)
    return jsonify({"cache": c, "ref": ref})


@app.before_request
def before_request():
    g.start_time = time.time()
    g.request_time = lambda: "%.5fs" % (time.time() - g.start_time)
    g.cache_status = cache.cache_status()


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")