from xml.dom import minidom
import json
from HTMLParser import HTMLParser
import requests
# from urlmocks import urldata

def processNode(node):
    banned = ["title", "hlword", "passage", "headline", "passages"]
    t = node.toxml()
    
    for b in banned: t = t.replace("<"+b+">", "").replace("</"+b+">", "") 
    for i in range(5): t = t.replace("  ", " ")
    return t


def parse(xmlstr):
    xmldoc = minidom.parseString(xmlstr)
    itemlist = xmldoc.getElementsByTagName('url')
    docs = xmldoc.getElementsByTagName('doc')
    arr = []
    h = HTMLParser()

    for doc in docs: 
        j = {}
        j["url"] = doc.getElementsByTagName('url')[0].firstChild.data
        j["title"] = processNode(doc.getElementsByTagName('title')[0])
        j["desc"] = ""        
        passages = doc.getElementsByTagName('passages')
        headline =  doc.getElementsByTagName('headline')

        if passages: j["desc"] += processNode(doc.getElementsByTagName('passages')[0])
        if passages and headline: j["desc"]  += "<br>"
        if headline: j["desc"] += processNode(doc.getElementsByTagName('headline')[0])
        # if j["url"] in urldata.keys():
        #     j["urldata"] = urldata[j["url"]]
        #     print j["url"] + " found" 
        j["desc"] = h.unescape(j["desc"])
        arr.append(j)


    arr = arr[:10]
    
    count = 0
    founddocs = xmldoc.getElementsByTagName('found-docs')

    if founddocs: count = founddocs[0].firstChild.nodeValue
    return {
        'result': arr,
        'count': count    
    }


def testdata(query):
    xmlstr = open('testdata/yandex/' + query + '.xml').read()
    res = parse(xmlstr)
    return res

def search(query, page=0):
    xmlstr = request(query,page)
    res = parse(xmlstr)
    return res

def request(query,page=0):
    url = "http://yandex.kodto.com/search/xml"

    querystring = {
        "user":"canera1990",
        "key":"03.336054239:5eb8ffcaadb7ebc3a3c40d11c801587f",
        #"query":"edirne%20n%C3%BCfus",
        "query":query,
        "page": page,
        "lr":"983","l10n":"tr",
        "sortby":"rlv","filter":"none",
        "groupby":"attr%3Dd.mode%3Ddeep.groups-on-page%3D10.docs-in-group%3D1"}

    payload = ""
    headers = {
        'Authorization': "Basic eWFuZGV4OjEyMzEyMw==",
        'cache-control': "no-cache",
        'Postman-Token': "db0de6b4-f33b-480f-a0f8-ae3221b4d820"
        }

    response = requests.request("GET", url, data=payload, headers=headers, params=querystring)

    # print(response.text)

    return response.text.encode('utf-8')

if __name__ == "__main__":
    data = testdata("westworld+altyazi")
    print json.dumps(data, indent=4)
    