import redis
import pickle 
import json 

r = redis.Redis(host='ara.kodto.com', port=6379, db=0, password="yandex123123")
cache_key = "opt:cache"
def get_cache(url):
    print "type", type(url)
    cc = r.get(cache_key) == "1"
    if not cc:
        print "opt cache kapali"
        return False
    s = r.get(u"cache:{}".format(url))
    if s:
        print json.dumps({"log": "cache hit", "url": url})
        return pickle.loads(s)
    else:
        print json.dumps({"log": "cache miss", "url": url})
        return None

def set_cache(url, val):
    s = pickle.dumps(val)
    r.set(u"cache:{}".format(url), s)
    print json.dumps({"log": "cache set", "url": url})

def toggle_cache():
    if r.get(cache_key) == "1":
        r.set(cache_key, 0)
        return False
    else:
        r.set(cache_key, 1)
        return True
        
def cache_status():
    if r.get(cache_key) == "1":
        return True
    else:
        return False

if __name__ == "__main__":
    v = {"a": "b", "c": {"d": 33}}
    # v = "dddd"
    set_cache("33", v)
    print get_cache("33")
    print "done"