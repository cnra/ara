from flask import Flask

app = Flask(__name__)

@app.route("/", methods=['GET', 'POST'])
def hello():
    return "r137 80 !!!"

if __name__ == "__main__":
    print "started"
    app.run(debug=True, port=5000, host="0.0.0.0")

